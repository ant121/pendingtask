import React from 'react';
import styled from 'styled-components';
import {Show} from '@styled-icons/boxicons-regular/Show';

const Container = styled.div`
  padding: 20px;
  width: 380px;
  font-family: Gill Sans, sans-serif;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.05), 0 0px 40px rgba(0, 0, 0, 0.08);
  border-radius: 7px;
`

const Card = styled.div`
  width: 375px;
  min-width: 300px;
  height: 200px;
  overflow: scroll;
  overflow-x: hidden;
  white-space: nowrap;
  display: flex;
  flex-direction: column;
  ::-webkit-scrollbar{
    width: 4px;
  }
  ::-webkit-scrollbar-track {
    background: white;
    border-radius: 20px;
  }
  ::-webkit-scrollbar-thumb {
    background: #CCCCCC;
    border-radius: 20px;
  }
`

const Title = styled.p`
  font-size: 22px;
  font-weight: bold;
  color: #437A99;
  margin: 0px 0px 15px;
`

const CardProject = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  border-left: solid;
  border-left-color: #437A99;
  border-left-width: 4px;
  padding: 0px 10px;
  margin-bottom: 15px;
`

const ProjectField = styled.div`
  display: flex;
  flex-direction: column;
`

const IconProject = styled(Show)`
  color: #437A99;
  cursor: pointer;
`

const TitleProject = styled.p`
  margin: 0px 0px 5px;
  color: #437A99;
`

const TitleProjectBold = styled.samp`
  margin: 0px;
  font-weight: bold;
  font-size: 18px;
  color: #453F43;
`

const TextProject = styled.p`
  margin: 0px;
  font-size: 14px;
  color: #453F43;
  font-weight: inherit;
`

const App = () => {
    return (
        <Container>
            <Title>Your pending tasks</Title>
            <Card>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
                <CardProject>
                    <ProjectField>
                        <TitleProject><TitleProjectBold>Project:</TitleProjectBold> Kleeper</TitleProject>
                        <TextProject>Lorem ipsum dolor sit amet.</TextProject>
                    </ProjectField>
                    <IconProject size="25"/>
                </CardProject>
            </Card>
        </Container>
    );
};

export default App;

